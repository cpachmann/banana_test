from typedb.driver import *
from uuid import uuid4
from datetime import datetime

def create_audit_entity_and_relations(repo_url):
    # Hardcoded account values
    account_uuid = "123"
    email = "test@test.test"

    # Generate a random UUID for the audit entity
    audit_uuid = str(uuid4())

    ai_synthesis = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

    try:
        with TypeDB.core_driver(TypeDB.DEFAULT_ADDRESS) as driver:
            with driver.session("CodeDD_test_2", SessionType.DATA) as session:
                # Check if account exists and insert if not
                with session.transaction(TransactionType.WRITE) as tx:
                    try:
                        account_query = f'match $account isa account, has account_uuid "{account_uuid}"; get $account;'
                        accounts = tx.query.match(account_query)
                        if not accounts:
                            insert_account_query = f'insert $account isa account, has account_uuid "{account_uuid}", has email "{email}";'
                            tx.query.insert(insert_account_query)
                        
                        # Create a new audit entity
                        insert_audit_query = f'''
                        insert $audit isa audit, 
                            has audit_uuid "{audit_uuid}", 
                            has ai_synthesis {ai_synthesis}, 
                            has audit_status "Opening";
                        '''
                        tx.query.insert(insert_audit_query)

                        # Commit the transaction to insert account and audit
                        tx.commit()
                    except Exception as e:
                        print(f"An error occurred during transaction: {e}")
                        tx.rollback()
                        raise

                # Create relations between account, audit, and git_repo
                with session.transaction(TransactionType.WRITE) as tx:
                    try:
                        # Insert the audit_trigger relation
                        insert_audit_trigger_query = f'''
                        match
                            $account isa account, has account_uuid "{account_uuid}";
                            $audit isa audit, has audit_uuid "{audit_uuid}";
                        insert (auditor: $account, triggered_audit: $audit) isa audit_trigger;
                        '''
                        tx.query.insert(insert_audit_trigger_query)

                        # Insert the audit_scope relation
                        insert_audit_scope_query = f'''
                        match
                            $audit isa audit, has audit_uuid "{audit_uuid}";
                            $repo isa git_repo, has url "{repo_url}";
                        insert (audit_review: $audit, reviewed_repo: $repo) isa audit_scope;
                        '''
                        tx.query.insert(insert_audit_scope_query)

                        # Commit the transaction to insert relations
                        tx.commit()
                    except Exception as e:
                        print(f"An error occurred during transaction: {e}")
                        tx.rollback()
                        raise

                with session.transaction(TransactionType.READ) as read_tx:
                    test_query = f'''
                    match
                        $audit isa audit, has audit_uuid "{audit_uuid}";
                        $repo isa git_repo, has url "{repo_url}";
                        (audit_review: $audit, reviewed_repo: $repo) isa audit_scope;
                    get $audit, $repo;
                    '''
                    result = list(read_tx.query.match(test_query))
                    if result:
                        print("Relation exists:", result)
                    else:
                        print("Relation not found right after insertion.")
                        
    except Exception as e:
        print(f"An error occurred while connecting to TypeDB or during session: {e}")

    return audit_uuid
